const VirtualSerialPort = require('udp-serial').SerialPort;
const five = require('johnny-five');

const { host } = require('./local-config.json');

const port = new VirtualSerialPort({
  host,
  type: 'udp4'
});
const board = new five.Board({ port });

board.once('ready', function() {
  console.log('five ready');

  let frontDistance = new five.Proximity({
    controller: "HCSR04",
    pin: 7
  });

  let backDistance = new five.Proximity({
    controller: "HCSR04",
    pin: 2
  });

  frontDistance.on("change", function() {
    console.log("front (cm): ", this.cm);
  });
  backDistance.on("change", function() {
    console.log("back (cm): ", this.cm);
  });

  this.repl.inject({
    frontDistance,
    backDistance
  });

});

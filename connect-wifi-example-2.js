const VirtualSerialPort = require('udp-serial').SerialPort;
const five = require("johnny-five");
const firmata = require('firmata');

const { host } = require('./local-config.json');

const sp = new VirtualSerialPort({
  host,
  type: 'udp4'
});
const io = new firmata.Board(sp);

io.once('ready', function() {
  console.log('IO Ready');
  io.isReady = true;

  var board = new five.Board({io, repl: true});

  board.on('ready', function() {
    console.log('five ready');

    // do magic here

  });
});
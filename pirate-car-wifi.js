const VirtualSerialPort = require('udp-serial').SerialPort;
const five = require("johnny-five");

const { host } = require("./local-config.json");

const port = new VirtualSerialPort({
  host,
  type: 'udp4'
});
const board = new five.Board({ port });

const MIN_SPEED = 90;
const MAX_SPEED = 250;

board.once('ready', function() {

  console.log('five ready');

  var debug = false;

  var state = {
    dir: 'fwd'   // direction of movement
  };

  var p10 = new five.Pin({ // stub for pin 10
    pin: 10,
    mode: 'input'
  });
  var motors = new five.Motors([
       { pins: { pwm: 6, dir: 12 } }, // pwm used to be 10. hack, getting rid of limitation due to unavailable pwm on 9 and 10 pins when using Servo library
       { pins: { pwm: 11, dir: 13 } }
  ]);

  var servo = new five.Servo(9);

  var frontDistance = new five.Proximity({
    controller: "HCSR04",
    pin: 7
  });

  var backDistance = new five.Proximity({
    controller: "HCSR04",
    pin: 2
  });

  var proximityHandlerMixin = function () {
    if (!(this instanceof proximityHandlerMixin)) {
      return new proximityHandlerMixin();
    }
    this.handler = 'auto',
    this.handlers = {},
    this.registerHandler = function (name, fn) {
      if (!this.handlers[name]) {
        this.handlers[name] = fn;
      }
    },
    this.removeHandler = function (name) {
      if (name === 'auto') {
        return;
      } else if (name === this.handler) {
        this.handler = 'auto'
      }
      this.handlers[name] = undefined;
    },
    this.switchHandler = function (name) {
      name = name || 'auto';
      if (this.handlers[name]) {
        console.log('switch handler from ' +this.handler+ ' to ' + name);
        this.handler = name
      }
    }
  };

  // front distance proximity
  Object.assign(frontDistance, new proximityHandlerMixin());

  frontDistance.registerHandler('auto', function () {
    debug && console.log('front: '+ this.cm);
    if (this.cm < 10) {
      if ((motors[0].isOn || motors[1].isOn) && state.dir === 'fwd') {
        motors.stop();
        console.log('frontDistance: force STOP', this.cm);
      }
    }
  });
  frontDistance.registerHandler('rotate', function() {
    if (this.cm < 4) {
      motors.stop();
      this.switchHandler();
    }
    return;
  });
  frontDistance.on("change", function() {
    this.handlers[this.handler] && this.handlers[this.handler].call(this);
  });

  // back distance proximity
  Object.assign(backDistance, new proximityHandlerMixin());
  backDistance.on("change", function() {
    if (typeof this.handlers[this.handler] === 'function') {
      this.handlers[this.handler].call(this)
    } else {
      console.log('unknow handler for back distance: ' + this.handler);
    }
  });
  backDistance.registerHandler('auto', function () {
    debug && console.log('back: '+ this.cm);
    if (this.cm < 12) {
      if ((motors[0].isOn || motors[1].isOn) && state.dir === 'rev') {
        motors.stop();
        console.log('backDistance: force STOP', this.cm);
      }
    }
  });
  backDistance.registerHandler('capture', function () {
    //console.log('capture: ' + this.cm);
    let holdDistance = 5; // cm
    if (this.cm > holdDistance) {
      motors.rev(MIN_SPEED);
    } else if (this.cm <= holdDistance) {
      motors.stop();
      servo.to(140);
      this.switchHandler('onhold');
    }
  });
  backDistance.registerHandler('onhold', function() {
    if (this.cm > 7) {
      this.switchHandler();
    }
    return;
  });
  backDistance.registerHandler('rotate', function() {
    if (this.cm < 7) {
      motors.stop();
      this.switchHandler();
    }
    return;
  });

  function capture() {
    backDistance.switchHandler('capture');
  }


  motors[0].on('forward', function() {
    state.dir = 'fwd';
  });
  motors[0].on('reverse', function() {
    state.dir = 'rev';
  });

  function turn(to, speed) {
    if (['left','right'].indexOf(to) < 0 ||
        ['fwd','rev'].indexOf(state.dir) < 0 ) {
      return;
    }

    speed = speed || MAX_SPEED;
    low_speed = MIN_SPEED;

    debug && console.log(to, state.dir)

    if ((to === 'right')) {
      motors[0][state.dir](speed);
      motors[1][state.dir](low_speed);
    } else if ((to === 'left')) {
      motors[1][state.dir](speed);
      motors[0][state.dir](low_speed);
    }

    board.wait(500, function() {
      motors.stop();
    });
  }

  function rotate(counterClockWise) {
    state.dir = 'rotate';
    if (backDistance.handler === 'auto') {
      backDistance.switchHandler('rotate');
      frontDistance.switchHandler('rotate');
    }
    if (counterClockWise) {
      motors[1].fwd(MAX_SPEED);
      motors[0].rev(MAX_SPEED);
    } else {
      motors[0].fwd(MAX_SPEED);
      motors[1].rev(MAX_SPEED);
    }
  }
  function rotateTime(time, counterClockWise) {
    rotate(counterClockWise);
    board.wait(time, function() {
      motors.stop();
    })
  }

  function stop() {
    motors.stop();
    frontDistance.switchHandler();
    backDistance.switchHandler();
  }

  let open = () => {
    servo.to(80);
    if (backDistance.handler === 'onhold') {
      backDistance.switchHandler();
    }
  };

  let close = () => {
    servo.to(180);
  };

  let hold = () => {
    servo.to(130);
  };

  this.repl.inject({
    //io, sp,
    motors,
    ctr: {open, close, hold},
    servo,
    // some more aliases
    m: motors,
    stop() {
      motors.stop();
    },
    right(speed) {
      turn('right', speed);
    },
    left(speed) {
      turn('left', speed);
    },
    rotate, rotateTime,
    setFwd() {
      state.dir = 'fwd';
    },
    setRev() {
      state.dir = 'rev';
    },
    capture,
    reset() {
      backDistance.switchHandler();
      state.dir = 'fwd';
    },
    forceOnhold() {
      backDistance.switchHandler('onhold');
    },
    turnAround(ccw) {
      rotateTime(1800, ccw);
    },
    turn90(ccw) {
      rotateTime(900, ccw);
    },
    debugEnable() {
      debug = true;
    },
    debugDisable() {
      debug = false;
    }
  });

});

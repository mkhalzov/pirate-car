const VirtualSerialPort = require('udp-serial').SerialPort;
const five = require('johnny-five');

const { host } = require('./local-config.json');

const port = new VirtualSerialPort({
  host,
  type: 'udp4'
});
const board = new five.Board({ port });

board.once('ready', function() {
  console.log('five ready');

  let motors = new five.Motors([
    { pins: { pwm: 6, dir: 12 } }, // pwm used to be 10. hack, getting rid of limitation due to unavailable pwm on 9 and 10 pins when using Servo library
    { pins: { pwm: 11, dir: 13 } }
  ]);

  const MAX_SPEED = 200;
  const MIN_SPEED = 100;

  let fwd = () => {
    motors[1].fwd(MAX_SPEED);
    motors[0].fwd(MAX_SPEED);
  }
  let rev = () => {
    motors[1].rev(MAX_SPEED);
    motors[0].rev(MAX_SPEED);
  }
  let stop = () => {
    motors.stop();
  }

  this.repl.inject({
    motors,
    fwd, rev,
    s: stop
  });

});

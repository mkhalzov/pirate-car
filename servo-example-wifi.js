const VirtualSerialPort = require('udp-serial').SerialPort;
const five = require('johnny-five');

const { host } = require('./local-config.json');

const port = new VirtualSerialPort({
  host,
  type: 'udp4'
});
const board = new five.Board({ port });

board.once('ready', function() {
  console.log('five ready');

  let servo = new five.Servo(9);

  servo.sweep();

  this.repl.inject({
    servo
  });

});

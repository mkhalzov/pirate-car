var five = require("johnny-five");
var board = new five.Board({debug: true});


board.on("ready", function() {
  var servo = new five.Servo(9);

  var m1 = new five.Motor({
    pins: {
      pwm: 10,
      dir: 12
    }
  });
  var m2 = new five.Motor({
    pins: {
      pwm: 11,
      dir: 13
    }
  });

/*
  var frontDistance = new five.Proximity({
    controller: "HCSR04",
    pin: 7
  });

  var backDistance = new five.Proximity({
    controller: "HCSR04",
    pin: 2
  });

  frontDistance.on("change", function() {
    //console.log("front (cm): ", this.cm);
  });
  backDistance.on("change", function() {
    console.log("back (cm): ", this.cm);
  });
*/

  // Sweep from 0-180 and repeat.
  //servo.sweep();

  let stop = () => {
    m1.stop();
    m2.stop();
  };

  let fwd = (speed) => {
    m1.fwd(speed);
    m2.fwd(speed);
  };

  let rev = (speed) => {
    m1.rev(speed);
    m2.rev(speed);
  }

  let brake = () => {
    m1.brake();
    m2.brake();
  }


  this.repl.inject({
    motors: {m1, m2},
    ctr: {stop, fwd, rev, brake},
    servo/*,
    distance: {front: frontDistance, back: backDistance}*/
  });

});
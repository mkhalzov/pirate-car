const VirtualSerialPort = require('udp-serial').SerialPort;
const five = require('johnny-five');

const songs = require('j5-songs');

const { host } = require('./local-config.json');

const port = new VirtualSerialPort({
  host,
  type: 'udp4'
});
const board = new five.Board({ port });

board.once('ready', function() {
  let piezo = new five.Piezo(4);

  // Load a song object
  let song = songs.load('mario-intro');

  // Play it !
  piezo.play(song);

  // List all songs
  songs.list(function (err, tunes) {
    // Object literal with all the songs
  });
});
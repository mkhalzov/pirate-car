# Overview

This is the source code for the Pirate cat (extended) controlled by Johnny Five.

## Hardware
- The [DFRobot Pirate 4WD Platform](https://www.dfrobot.com/product-97.html)
- [Arduino Uno](https://store.arduino.cc/arduino-uno-rev3)
- 2x2A [Motor Shield L298P](http://mini-tech.com.ua/download/datasheet/mechanic/L298P-Motor-Shield-Instruction.pdf) 
- 4 rotor motors (connected as 2x2), 
- [Robot-crew](http://arduino-ua.com/prod538-Robotizirovannaya_kleshnya_MKII) which is powered by 
- [Servo motor MG996R](http://www.mini-tech.com.ua/index.php?route=product/product&path=133_61&product_id=662)
- [ultrasonic distance sensor](http://www.mini-tech.com.ua/index.php?route=product/product&filter_name=%D1%80%D0%B0%D1%81%D1%81%D1%82%D0%BE%D1%8F%D0%BD%D0%B8%D1%8F&product_id=66)
- [esp-12](https://arduino-ua.com/prod1379-wi-fi-modyl-esp8266-versiya-esp-12e), a esp8266 wifi module connected in order to controll the car wirelessly.

# Upload Arduino Firmata

[PingFirmata](https://gist.githubusercontent.com/rwaldron/0519fcd5c48bfe43b827/raw/f17fb09b92ed04722953823d9416649ff380c35b/PingFirmata.ino)

Modify this line Firmata.begin(57600); with Firmata.begin(115200);

Upload to the Arduino using Arduino IDE